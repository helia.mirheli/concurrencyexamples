#FROM ubuntu:latest
#
## Update the package lists and install necessary dependencies
#RUN apt-get update && apt-get install -y openjdk-17-jdk curl
#
## Download and install Maven
#RUN curl -fsSL https://dlcdn.apache.org/maven/maven-3/3.9.5/binaries/apache-maven-3.9.5-bin.tar.gz -o /tmp/apache-maven.tar.gz \
#    && tar xzf /tmp/apache-maven.tar.gz -C /opt \
#    && ln -s /opt/apache-maven-3.9.5 /opt/maven \
#    && rm /tmp/apache-maven.tar.gz
#
## Set the JAVA_HOME and PATH environment variables
#ENV JAVA_HOME /usr/lib/jvm/java-17-openjdk-amd64
#ENV PATH $JAVA_HOME/bin:/opt/maven/bin:$PATH
#
## Optionally, you can set the Maven repository mirror URL
## ENV MAVEN_OPTS -Dmaven.repo.local=/path/to/maven/repository
#
## Set the working directory
#WORKDIR /app
#
## Copy your application's source code to the container
#COPY . /app
#
## Build your application with Maven
#RUN mvn -U clean install -q
#
## Specify the command to run your application
##CMD ["java", "-jar", "/app/target/concurrencyExamples-1.0-SNAPSHOT.jar"]
