package generateCarThreadSafeSample;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class VehicleProduction {

    private final Lock lock = new ReentrantLock();
    private boolean body;
    private boolean equipments;
    private boolean engine;
    private boolean painting;
    private boolean shipping;

    public VehicleProduction() {
        this.body = false;
        this.equipments = false;
        this.engine = false;
        this.painting = false;
        this.shipping = false;
    }

    public void manufactureBody() {
        lock.lock();
        try {
            if (!body) {
                System.out.println("Manufacturing Body Of The Car ...");
                Thread.sleep(1000);
                body = true;
                System.out.println("Body Manufactured Successfully!");
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            lock.unlock();
        }
    }

    public void assembleEquipments() {
        lock.lock();
        try {
            if (!equipments) {
                System.out.println("Assembling Equipments On The Car ...");
                Thread.sleep(1000);
                equipments = true;
                System.out.println("Equipments Assembled!");
            }

        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            lock.unlock();
        }
    }

    public void assembleEngine() {
        lock.lock();
        try {
            if (!engine) {
                System.out.println("Assembling Engine ...");
                Thread.sleep(1000);
                engine = true;
                System.out.println("Engine Assembled!");
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            lock.unlock();
        }
    }

    public void paint() {
        lock.lock();
        try {
            if (!painting) {
                System.out.println("Painting The Car ...");
                Thread.sleep(1000);
                painting = true;
                System.out.println("Car Painted!");
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            lock.unlock();
        }
    }

    public void shipVehicle() {
        lock.lock();
        try {
            if (!shipping) {
                System.out.println("Vehicle Ready For Shipping ...");
                Thread.sleep(1000);
                shipping = true;
                System.out.println("Vehicle Shipped");
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            lock.unlock();
        }
    }

    public void generateCar() {
        manufactureBody();
        assembleEquipments();
        assembleEngine();
        paint();
        shipVehicle();
        System.out.println("Car Generated.");
    }
}
