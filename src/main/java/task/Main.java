package task;

public class Main {
    public static void main(String[] args) {
        String inputString = "21-22-4-5-6-7-23";
        findSequenceOfFour(inputString);
    }

    public static void findSequenceOfFour(String inputString) {
        String[] elements = inputString.split("-");
        for (int i = 0; i < elements.length - 4; i++) {
            boolean isSequence = true;
            for (int j = i; j < i + 3; j++) {
                int currentNumber = Integer.parseInt(elements[j]);
                int nextNumber = Integer.parseInt(elements[j + 1]);
                if (nextNumber - currentNumber != 1) {
                    isSequence = false;
                    break;
                }
            }
            if (isSequence) {
                for (int j = i; j <= i + 3; j++) {
                    System.out.print(elements[j]);
                    if (j < i + 3) {
                        System.out.print("-");
                    }
                }
            }
        }
    }
}
