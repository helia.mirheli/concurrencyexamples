package geeksForgeeksThreadPool;

import java.text.SimpleDateFormat;
import java.util.Date;

//Task Class to be executed
public class Task implements Runnable {
    private final String name;

    public Task(String s) {
        this.name = s;
    }

    @Override
    public void run() {
        try {
            Date date = new Date();
            SimpleDateFormat ft = new SimpleDateFormat("hh:mm:ss");
            for (int i = 0; i <= 5; i++) {
                if (i == 0) {
                    System.out.println("Initialization time for"
                            + " task name" + name + " = " + ft.format(date));
                } else {
                    System.out.println("Executing Time For Task Name " + name + " = " + ft.format(date));
                }
                Thread.sleep(1000);
            }
            System.out.println(name + " complete");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
