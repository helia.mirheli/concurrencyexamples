package BaeldungExamples.threadSafetyApproaches.statelessImplementations;

import java.math.BigInteger;

/*
* Given a specific input, it always produces the same output.
 The method neither relies on external state nor maintains state at all.
*  So, it’s considered to be thread-safe and can be safely called by multiple threads at the same time.
* */
public class MathUtils {
    public static BigInteger factorial(int number) {
        BigInteger f = new BigInteger("1");
        for (int i = 2; i < number; i++) {
            f = f.multiply(BigInteger.valueOf(i));
        }
        return f;
    }
}
