package BaeldungExamples.threadSafetyApproaches.synchronizedStatements;

public class SyncStatementCounter {
    private int counter = 0;

    public void incrementCounter() {
        synchronized (this) {
            counter += 1;
        }
    }
}
