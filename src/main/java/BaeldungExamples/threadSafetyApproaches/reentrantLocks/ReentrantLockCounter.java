package BaeldungExamples.threadSafetyApproaches.reentrantLocks;

import java.util.concurrent.locks.ReentrantLock;

//optional fairness boolean parameter. When set to true, and multiple threads are trying to acquire a lock,
// the JVM will give priority to the longest waiting thread and grant access to the lock.

public class ReentrantLockCounter {
    private final ReentrantLock reLock = new ReentrantLock(true);
    private int counter;

    public void incrementCounter() {
        reLock.lock();
        try {
            counter += 1;
        } finally {
            reLock.unlock();
        }
    }
}
