package BaeldungExamples.threadSafetyApproaches.threadLocalFields;

public class StateHolder {
    private final String state;

    public StateHolder(String state) {
        this.state = state;
    }

    @Override
    public String toString() {
        return "{" +
                '\'' + state + '\'' +
                '}';
    }
}
