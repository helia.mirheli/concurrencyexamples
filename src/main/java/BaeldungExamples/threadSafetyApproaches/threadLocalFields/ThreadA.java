package BaeldungExamples.threadSafetyApproaches.threadLocalFields;

import java.util.Arrays;
import java.util.List;

//We can easily create classes whose fields are thread-local by simply defining private fields in Thread classes.
public class ThreadA extends Thread {
    private final List<Integer> numbers = Arrays.asList(1, 2, 3, 4, 5);

    @Override
    public void run() {
        numbers.forEach(System.out::println);
    }
}
