package BaeldungExamples.threadSafetyApproaches.threadLocalFields;

import java.util.Arrays;
import java.util.List;

//the classes have their own state, but it’s not shared with other threads. So, the classes are thread-safe.
public class ThreadB extends Thread {
    private final List<String> letters = Arrays.asList("a", "b", "c", "d", "e");

    @Override
    public void run() {
        letters.forEach(System.out::println);
    }
}
