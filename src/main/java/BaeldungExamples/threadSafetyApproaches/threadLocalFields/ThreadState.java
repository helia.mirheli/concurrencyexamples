package BaeldungExamples.threadSafetyApproaches.threadLocalFields;


/*
making StateHolder class a thread-local variable
Thread-local fields are pretty much like normal class fields, except that each thread that accesses them
via a setter/getter gets an independently initialized copy of the field so that each thread has its own state
* */

public class ThreadState {

    public static final ThreadLocal<StateHolder> statePerThread = new ThreadLocal<>() {
        protected StateHolder initialValue() {
            if (Thread.currentThread().getId() == 14) {
                return new StateHolder("active");
            } else {
                return new StateHolder("inactive");
            }
        }
    };

    public static void main(String[] args) {
        for (int i = 0; i < 5; i++) {
            Thread thread = new Thread(() -> {
                StateHolder value = statePerThread.get();
                System.out.println("Thread " + Thread.currentThread().getId() + " initial value : " + value.toString());
            });
            thread.start();
        }
    }
}