package BaeldungExamples.threadSafetyApproaches.synchronizedMethods;

public class SynchronizedCounter {
    private int counter = 0;

    // additional un synced operations

    public synchronized void incrementCounter() {
        counter += 1;
    }
}
