package BaeldungExamples.threadSafetyApproaches.volatileFied;

//if a thread reads the value of a volatile variable, all the variables visible to the thread will be read from the main memory too.
public class Counter {
    private static volatile int counter=0;

    public static void main(String[] args) {
        for (int i = 0; i < 5; i++) {
            Thread thread = new Thread(() -> {
                int value = counter++;
                System.out.println("Thread " + Thread.currentThread().getId() + " initial value: " + value);
            });
            thread.start();
        }
    }
}
