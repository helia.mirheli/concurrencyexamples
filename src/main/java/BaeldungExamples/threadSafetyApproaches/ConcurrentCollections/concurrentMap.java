package BaeldungExamples.threadSafetyApproaches.ConcurrentCollections;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
// several threads can acquire locks on different map segments, so multiple threads can access the Map at the same time.

public class concurrentMap {
    public static void main(String[] args) {
        Map<String, String> concurrentMap = new ConcurrentHashMap<>();
        concurrentMap.put("1", "one");
        concurrentMap.put("2", "two");
        concurrentMap.put("3", "three");
    }

}
