package BaeldungExamples.threadSafetyApproaches.AtomicObjects;

//Atomic classes allow us to perform atomic operations, which are thread-safe, without using synchronization
// An atomic operation is executed in one single machine-level operation.

import java.util.concurrent.atomic.AtomicInteger;

public class AtomicCounter {
    private final AtomicInteger counter = new AtomicInteger();

    public void incrementCounter() {
        counter.incrementAndGet();
    }

    public int getCounter() {
        return counter.get();
    }
}
