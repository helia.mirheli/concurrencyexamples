package BaeldungExamples.threadSafetyApproaches.immutableImpelementations;

//immutable class in java is by declaring all the fields private and final and not providing setters
public class MessageService {
private final String message;

    public MessageService(String message) {
        this.message = message;
    }

    //getter
}
