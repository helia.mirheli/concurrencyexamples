package BaeldungExamples.threadSafetyApproaches.Lock;

public class ObjectLockCounter {
    private final Object lock = new Object();
    private int counter = 0;

    public void incrementCounter() {
        synchronized (lock) {
            counter += 1;
        }
    }
}
