package EffectiveJavaExamples.concurrencyUtilities;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;


public class StringInternSimulator {
    private static final ConcurrentMap<String, String> map = new ConcurrentHashMap<>();
    //not Optimal
    public static String intern(String s) {
        map.put(String.valueOf(1),"Mina");
        String previousValue = map.putIfAbsent(s, s);
        return previousValue == null ? s : previousValue;
    }

}
