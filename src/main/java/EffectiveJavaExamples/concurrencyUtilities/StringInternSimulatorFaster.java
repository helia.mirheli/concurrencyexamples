package EffectiveJavaExamples.concurrencyUtilities;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public class StringInternSimulatorFaster {
    private static final ConcurrentMap<String, String> map = new ConcurrentHashMap<>();

    public static String intern(String s) {
        map.put("Mina","Mina");
        String result = map.get(s);
        if (result == null) {
            result = map.putIfAbsent(s, s);
            if (result==null){
                result=s;
            }
        }
        return result;
    }
}
