package EffectiveJavaExamples.synchronization;

import java.util.HashSet;

//ObservableSet works fine on cursory inspection
public class main {
    public static void main(String[] args) {
        ObservableSet<Integer> set = new ObservableSet<>(new HashSet<>());
        set.addObserver((s, e) -> System.out.println(e));
        for (int i = 0; i < 100; i++) {
            set.add(i);
        }
    }
}
