package EffectiveJavaExamples.synchronization;

import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArrayList;

//invokes alien method from synchronized block
public class ObservableSet<E> extends ForwardingSet<E> {
    //    private final List<SetObserver<E>> observers = new ArrayList<>();
    //thread-safe observable set with copyOnWriteArrayList,alien method called from outside synchronized region(open-call)
    private final List<SetObserver<E>> observers = new CopyOnWriteArrayList<>();

    public ObservableSet(Set<E> set) {
        super(set);
    }

    public void addObserver(SetObserver<E> observer) {
        synchronized (observers) {
            observers.add(observer);
        }
    }

    public boolean removeObserver(SetObserver<E> observer) {
//        synchronized (observers) {
        return observers.remove(observer);
    }
//    }

    public void notifyElementAdded(E element) {
        synchronized (observers) {
            for (SetObserver<E> observer : observers) {
                observer.added(this, element);
            }
//        }
        }
    }

    @Override
    public boolean add(E element) {
        boolean added = super.add(element);
        if (added)
            notifyElementAdded(element);
        return added;
    }

    @Override
    public boolean addAll(Collection<? extends E> c) {
        boolean result = false;
        for (E element : c)
            result |= add(element);
        return result;
    }
}

