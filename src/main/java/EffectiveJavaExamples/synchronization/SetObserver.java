package EffectiveJavaExamples.synchronization;
//(subscribers)invoked when an element is added to the observable set
@FunctionalInterface public interface SetObserver<E> {
    void added(ObservableSet<E> set,E element);

}
