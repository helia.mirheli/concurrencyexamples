package EffectiveJavaExamples.synchronization;

import java.util.HashSet;

/*
 * unsubscribing
 * with anonymous class instance
 * which throws concurrentModification Exception
 * due to removing an element from the list
 * in the midst of iterating over it
 * */
public class removeObserver {
    public static void main(String[] args) {
        ObservableSet<Integer> set = new ObservableSet<>(new HashSet<>());
        set.addObserver(new SetObserver<>() {
            @Override
            public void added(ObservableSet<Integer> s, Integer e) {
                System.out.println(e);
                if (e == 23) {
                    s.removeObserver(this);
                }
            }
        });
        for (int i = 0; i < 100; i++) {
            set.add(i);
        }
    }
}
