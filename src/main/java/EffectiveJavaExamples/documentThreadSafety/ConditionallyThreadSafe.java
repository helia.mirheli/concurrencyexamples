package EffectiveJavaExamples.documentThreadSafety;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

//it is important to synchronize manually  on the returned map when iterating over its collection vies
public class ConditionallyThreadSafe {

    Map<Object, Object> m= Collections.synchronizedMap(new HashMap<>());
    Set<Object> s=m.keySet();

//    synchronized(m){ //synchronizing on m, not s
//        for (Object key:s)
//            key.toString();
//    }
}
