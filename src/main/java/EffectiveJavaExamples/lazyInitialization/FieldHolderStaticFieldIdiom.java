package EffectiveJavaExamples.lazyInitialization;

//Lazy Initialization holder class idiom for static fields
//the beauty of this idiom is that getField method is not synchronized and
//performs only a field access

public class FieldHolderStaticFieldIdiom {
    private static String computeFieldValue() {
        return null;
    }

    private static String getField() {
        return FieldHolder.field;
    }

    private static class FieldHolder {
        static final String field = computeFieldValue();
    }
}

