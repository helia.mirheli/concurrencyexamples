package EffectiveJavaExamples.lazyInitialization;

public class DoubleCheckIdiomForInstanceFields {
    private volatile String field;

    private String getField() {
        String result = field;
        if (result == null) {//firstCheck no locking
            synchronized (this) {
                if (field == null)//second Check with locking
                    field = result = computeFieldValue();
            }
        }
        return result;
    }

    private String computeFieldValue() {
        return null;
    }
}
