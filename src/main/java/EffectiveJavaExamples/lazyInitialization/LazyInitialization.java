package EffectiveJavaExamples.lazyInitialization;

public class LazyInitialization {
    //normal initialization
//    private final String field=computeFieldValue();
//Lazy Initialization
    private String field;
    private synchronized String getField(){
        if (field==null)
            field=computeFieldValue();
        return field;
    }
    private String computeFieldValue() {
        return field;
    }
}
