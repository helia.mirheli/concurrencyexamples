import generateCarThreadSafeSample.VehicleProduction;

public class CarAssembly {
    public static void main(String[] args) {
        VehicleProduction vehicle = new VehicleProduction();

        Runnable assemblyTask = () -> vehicle.generateCar();

        Thread thread1 = new Thread(assemblyTask);
        Thread thread2 = new Thread(assemblyTask);
        Thread thread3 = new Thread(assemblyTask);

        thread1.start();
        thread2.start();
        thread3.start();
    }
}
